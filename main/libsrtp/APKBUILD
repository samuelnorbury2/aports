# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libsrtp
pkgver=2.4.1
pkgrel=0
pkgdesc="implementation of the Secure Real-time Transport Protocol (SRTP)"
url="http://srtp.sourceforge.net"
arch="all"
license="BSD-3-Clause"
makedepends="automake autoconf libtool"
subpackages="$pkgname-static $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/cisco/libsrtp/archive/v$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make all shared_library
}

package() {
	make DESTDIR="$pkgdir" install
}

check() {
	# Required for loading libsrtp2.so.1
	LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$builddir" make runtest
}

static() {
	depends=""
	pkgdesc="$pkgdesc (static library)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib
}

sha512sums="
24917a9656dfff975cee789696c9163c21b0db13771c3989337b5348bc3ef342056935a3f87c7ebf129bdc27cf7e66f80f6b04b8decdf5065e275652b6f4184b  libsrtp-2.4.1.tar.gz
"
