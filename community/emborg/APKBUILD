# Contributor: Andy Hawkins <andy@gently.org.uk>
# Maintainer: Andy Hawkins <andy@gently.org.uk>
pkgname=emborg
pkgver=1.26
pkgrel=1
pkgdesc="Front-End to Borg Backup"
url="https://emborg.readthedocs.io/"
arch="noarch !s390x !mips !mips64 !armhf" # limited by borgbackup
license="GPL-3.0-or-later"
depends="
	borgbackup
	python3
	py3-appdirs
	py3-arrow
	py3-docopt
	py3-inform
	py3-quantiphy
	py3-requests
	py3-shlib
	"
makedepends="py3-setuptools"
checkdepends="
	py3-pytest
	py3-nestedtext
	py3-parametrize-from-file
	py3-voluptuous
	"
source="https://github.com/KenKundert/emborg/archive/v$pkgver/emborg-v$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	MISSING_DEPENDENCIES="fuse" PYTHONPATH="$PWD/build/lib" pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
a02f8666d355f95d664c02102937c332ffddfe65a2d26d8d85671ae80c592509159ed2f7399eccbcf7e3ae4170d26ed2b8e6bd86a2fc81213e71709b8dd485a1  emborg-v1.26.tar.gz
"
